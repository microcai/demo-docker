FROM ubuntu:wily
MAINTAINER alaudadoc alaudadoc@alauda.cn
VOLUME /log

RUN apt-get update && apt-get install -y libpq5 libprotobuf9v5 mysql-client
EXPOSE 80
EXPOSE 8878

COPY voe-master /
RUN chmod +x /voe-master
WORKDIR /log
CMD ["/voe-master", "--web-no-auth", "--webport", "80", "--webbind", "0.0.0.0", "--db_backend", "sqlite3"]
